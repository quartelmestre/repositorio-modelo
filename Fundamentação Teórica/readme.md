# Regras gerais

1. O orientador tem razão.

2. O orientador tem sempre razão.

3. Na improvável hipótese de o orientador não ter razão, volte para a regra 1.

# Regras específicas

1. A banca tem razão.

2. A banca tem sempre razão.

3. Na improvável hipótese de a banca não ter razão, volte para a regra 1.

# Consolidação das regras gerais e específicas

**Manda quem pode, obedece quem tem juízo.**
