# Instalação do *software*

## Instalando o Visual Studio Code

O VSCode, ou apenas Code, é um ambiente de edição, desenvolvido pela Microsoft. Ele será o principal editor de textos usado no projeto.

Para começar, abra a página [https://code.visualstudio.com/](https://code.visualstudio.com/). Clique em *Download for Windows* e baixe o arquivo de instalação.

![07.png](07.png)

Execute o programa. Aceite todas as opções de instalação. Ao final, desabilite a opção de executar o programa.

![08.png](08.png)

O VSCode está instalado, mas ainda não é hora de usá-lo. Antes disso, vamos instalar outro programa.

## Instalando o Git for Windows

O ```git``` é um programa para realizar **controle de versões** de documentos -- ou seja, por meio desta ferramenta é possível preservar e recuperar versões anteriores de documentos nos quais trabalhamos.

Isto é uma forma de *back-up*, ou cópia de segurança, mas vai além de uma simples cópia. Não vou dar maiores explicações aqui; você pode ler um pouco mais sobre o ```git``` em dois artigos que [publiquei em meu blog](https://lcduarte.com/2020/02/20/organizando-minha-pesquisa-parte-1/).

Abra a página [https://gitforwindows.org/](https://gitforwindows.org/).

![01.png](01.png)

Clique em "Download"; quando acabar de receber o arquivo, execute-o. Após autorizar a execução, a primeira tela é a licença GNU. Pode aceitá-la.

![02.png](02.png)

Há uma grande quantidade de configurações. Para quase todas, aceite a opção padrão. Somente uma delas precisa de alteração: quando aparecer a opção de editor (*Choosing the default editor used by Git*), selecione a opção "Use Visual Studio Code as Git's default editor".

![06.png](06.png)

Encerrada a instalação, desabilite a opção de execução e a opção de ler as notas da versão, antes de fechar o programa de instalação.

![21.png](21.png)

## Configurando o VSCode

Agora que o ```git``` está instalado, podemos configurar o VSCode. É importante notar que este procedimento de configuração serve para preparar o ambiente de edição, e é realizado uma única vez (a não ser que você precise instalar estes programas novamente).

Abra o VSCode. Se quiser, selecione um tema de cores diferente, antes de continuar. Recomendo maximizar a janela.

![22.png](22.png)

Na barra de comandos, clique em "Terminal" e em "New terminal".

![24.png](24.png)

Uma linha de comando Powershell vai se abrir na parte inferior da tela.

![25.png](25.png)

Do lado direito da tela, embaixo, clique na setinha para baixo e selecione a opção "Git Bash".

![26.png](26.png)

Isto vai abrir uma nova janela de terminal, por cima do terminal Powershell anterior.

![27.png](27.png)

Na mesma setinha para baixo, agora selecione a opção "Select Default Profile".

![28.png](28.png)

Vai se abrir um menu ao alto. Selecione nele a opção "Git Bash".

Os procedimentos acima configuraram o VSCode para usar janelas de comando do Git for Windows, que instalamos anteriormente. Agora vamos configurar o próprio Git for Windows, de dentro do terminal do VSCode.

Dois comandos identificam você no Git. Copie os dois comandos abaixo, um de cada vez, e cole-os no terminal Bash que está aberto no fim da tela do VSCode. Mude os textos em maiúsculas para o seu nome e o seu endereço de *e-mail*, conforme indicado, mas mantendo-os entre aspas. Ao final de cada linha, tecle **Enter**.

- ```git config --global user.name "SEU NOME"```
- ```git config --global user.email "SEU E-MAIL"```

![30.png](30.png)

Antes de prosseguirmos com a configuração do Git, vamos criar uma conta no GitLab. Deixe o VSCode aberto (pode minimizá-lo, se quiser) e abra seu navegador.

## Criando uma conta no GitLab

Abra a página [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up). Preencha seu prenome (*First name*), sobrenome (*Last name*); escolha um nome de usuário (*Username*); informe seu endereço de *e-mail* e crie uma senha (*Password*).

![Screenshot_20220120_103504.png](Screenshot_20220120_103504.png)

O GitLab vai lhe enviar uma mensagem de *e-mail*, para confirmar a validade do seu endereço.

![Screenshot_20220120_123941.png](Screenshot_20220120_123941.png)

Abra seu gerenciador de *e-mail*, e faça a confirmação. Depois disso, você pode entrar no GitLab com o nome de usuário e a senha que escolheu.

Em sua primeira tela, o GitLab pede algumas informações sobre o seu papel e o motivo de estar usando o servidor.

![Screenshot_20220120_124123.png](Screenshot_20220120_124123.png)

Eu recomendo selecionar as opções "Other" (outros) e 

![Screenshot_20220120_124208.png](Screenshot_20220120_124208.png)

Depois disso, selecione ao final a opção "" e clique em "Continue".

![Screenshot_20220120_124224.png](Screenshot_20220120_124224.png)

A próxima tela oferece a opção de criar um novo projeto. Minimize seu navegador e retorne ao VSCode.

![Screenshot_20220120_124304.png](Screenshot_20220120_124304.png)

## De volta ao VSCode

Novamente no terminal, cope e cole o comando abaixo. Use o mesmo endereço de *e-mail* que você no comando anterior.

- ```ssh-keygen -o -t rsa -b 4096 -C "SEU E-MAIL"```

Tecle **Enter** para todas as perguntas, até aparecer uma sequência de caracteres como esta:

![32.png](32.png)

A sua chave de criptografia foi criada. Agora, copie e cole no terminal este comando:

- ```cat ~/.ssh/id_rsa.pub | clip```

Este comando copia a chave criada, e permite colá-la em outro lugar. Então, de volta ao navegador, abra a página [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys).

![Screenshot_20220120_123229.png](Screenshot_20220120_123229.png)

Clique no grande campo "Key", e cole nele a chave que foi copiada do VSCode. Vai aparecer uma verdadeira sopa de letrinhas.

Abaixo do campo, dê um nome ("Title") à sua chave (por exemplo, "chave do VSCode"). Se quiser, marque no calendário uma data de validade para ela ("Expiration date"), mas isso não é necessário. A seguir, clique no botão "Add key".

Sua chave de criptografia foi copiada do VSCode para o GitLab. Agora, você consegue enviar um documento qualquer do VSCode para o GitLab, sem precisar sequer abrir o GitLab em um navegador, e sem mesmo ter que informar senha.

## Próximos passos

Ok, tudo instalado, e a configuração mínima foi feita. O próximo passo é entrar no projeto que estamos desenvolvendo. Mande para mim o seu nome de usuário do GitLab, o mesmo que você usa para entrar na página, e eu vou acrescentá-lo aos participantes do projeto.
